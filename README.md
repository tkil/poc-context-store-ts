# poc-context-store-ts
#### Repo: https://gitlab.com/tkil/poc-context-store-ts
#### Demo: https://tkil.gitlab.io/poc-context-store-ts/
#### Deploy Job: gitlab-ci via .gitlab-ci.yml

### Summary
A POC / demo of what a data store looks like using context API
* Context API is built into react and does not require an additional dep
* Less verbose than redux
* The context itself is a bit busy, but most of this is to enforce TS typing and graceful defaults
* "Connected Pattern" is used similar to the `mapStateToProps` and `mapDispatchToProps`

### Technologies Used
* React
* ContextApi

### Prerequisites
Node is installed

### Installation
```
npm install
```

### Running Project
```
npm start
```

### Running Tests
Only basic "works without exploding" tests implemented
```
npm test
```

### CI / Deployment
```
npm run deploy
```

### Authors
* Tyler Kilburn <tylerkilburn@gmail.com>
