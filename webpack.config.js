// Tools
const path = require("path");

// Plugins
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

// Variables - Directory Target
const dir = path.resolve(__dirname);
const srcDir = path.join(dir, "src");
const distDir = path.join(dir, "dist");
const nodeModulesDir = path.join(dir, "node_modules");

// Variables - File Targets
const srcIndex = "index.ts";
const srcTemplate = "index.html";
const distHtml = "index.html";

// Variables - Optimization
const webpackMode = process.env.NODE_ENV === "production" ? "production" : "development";
const isProduction = webpackMode === "production";

// Variables - Other
const devPort = process.env.DEV_SERVER_PORT || 8080;

// Main Webpack Config
module.exports = {
  target: "web",
  mode: webpackMode,
  entry: path.join(srcDir, srcIndex),
  devServer: {
    compress: true,
    port: devPort,
    open: true,
    openPage: distHtml,
  },
  output: {
    path: path.resolve(distDir),
    filename: "[name].[contenthash].js",
  },
  resolve: {
    alias: [],
    extensions: [".js", ".jsx", ".ts", ".tsx", ".json"],
    mainFields: ["browser", "main", "module"],
    plugins: [new TsconfigPathsPlugin({ configFile: path.join(dir, "tsconfig.json") })],
  },
  module: {
    rules: [
      {
        test: /.(ts|tsx)$/,
        use: "ts-loader",
        include: dir,
        exclude: [nodeModulesDir, /.test.tsx?$/],
      },
      {
        test: /.(sa|sc|c)ss$/,
        include: dir,
        enforce: "pre",
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: { importLoaders: 1, sourceMap: !isProduction, url: false },
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: () => [
                require("autoprefixer")(),
                require("postcss-import")(),
                require("postcss-preset-env")({ stage: 3 }),
                isProduction
                  ? require("cssnano")({
                      preset: [
                        "default",
                        {
                          discardComments: { removeAll: true },
                        },
                      ],
                    })
                  : () => {},
              ],
            },
          },
        ],
      },
      {
        test: /.(png|jp(e*)g|svg|ico)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 512,
              name: "images/[hash]-[name].[ext]",
            },
          },
        ],
        exclude: nodeModulesDir,
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: distHtml,
      template: path.join(srcDir, srcTemplate),
      // minify: { collapseWhitespace: true }
    }),
    new MiniCssExtractPlugin({
      filename: "main.[contenthash].css",
      chunkFilename: "[name].[contenthash].css",
    }),
  ],
};
