// Docs: https://jestjs.io/docs/en/configuration.html

module.exports = {
  clearMocks: true,
  collectCoverage: false,
  collectCoverageFrom: [
    // Match Pattern
    "**/*.{js,jsx,ts,tsx}",
    // Ignore Directories
    "!**.expected.**",
    "!**/__tests__/**",
    "!**/assets/**",
    "!**/bin/**",
    "!**/cli/**",
    "!**/coverage/**",
    "!**/dist/**",
    "!**/env/**",
    "!**/node_modules/**",
    "!**/tests/**",
    "!**/types/**",
    "!**/vendor/**",
    // Ignore Files
    "!**/**.config.**",
  ],
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: ["/cli/", "/node_modules/"],
  globals: {
    "ts-jest": {
      tsConfig: "tsconfig.json",
    },
  },
  moduleFileExtensions: ["js", "jsx", "ts", "tsx", "vue", "json"],
  // setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    ".*.(css|scss)$": "<rootDir>/src/testing/testStub.ts",
    "@env/(.*)": "<rootDir>/env/$1",
    "@components/(.*)": "<rootDir>/src/components/$1",
    "@stores/(.*)": "<rootDir>/src/stores/$1",
    "@modules/(.*)": "<rootDir>/node_modules/$1",
  },
  modulePathIgnorePatterns: [],
  testMatch: [
    "**/index.test.(js|jsx|ts|tsx)",
    "**/__tests__/*test.+(js|jsx|ts|tsx)",
    "**/tests/integration/*test.+(js|jsx|ts|tsx)",
  ],
  testPathIgnorePatterns: ["/node_modules/"],
  transformIgnorePatterns: ["<rootDir>/node_modules/"],
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
};
