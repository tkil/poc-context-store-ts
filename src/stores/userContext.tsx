import React, { createContext, useState } from "react";

// Types
export interface IUserContextState {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}

export interface IUserContextActions {
  setFirstName: (firstName: string) => void;
  setLastName: (lastName: string) => void;
  setEmail: (email: string) => void;
  setPhone: (phone: string) => void;
}

export interface IUserContext {
  state: IUserContextState;
  actions: IUserContextActions;
}

// Init
export const userContextEmptyState: IUserContextState = {
  firstName: "",
  lastName: "",
  email: "",
  phone: "",
};

const emptyAction = () => {};
export const userContextEmptyActions: IUserContextActions = {
  setFirstName: emptyAction,
  setLastName: emptyAction,
  setEmail: emptyAction,
  setPhone: emptyAction,
};

export const UserContext = createContext<IUserContext>({
  state: userContextEmptyState,
  actions: userContextEmptyActions,
});

// Implementation
interface IUserContextProviderParams {
  children: JSX.Element[] | JSX.Element;
  initialState?: IUserContextState;
}

export const UserContextConsumer = UserContext.Consumer;

export const UserContextProvider = ({ children, initialState = userContextEmptyState }: IUserContextProviderParams) => {
  const [state, setState] = useState<IUserContextState>(initialState);

  const actions: IUserContextActions = {
    setFirstName: (firstName: string) => {
      setState({ ...state, firstName });
    },
    setLastName: (lastName: string) => {
      setState({ ...state, lastName });
    },
    setEmail: (email: string) => {
      setState({ ...state, email });
    },
    setPhone: (phone: string) => {
      setState({ ...state, phone });
    },
  };

  return <UserContext.Provider value={{ actions, state }}>{children}</UserContext.Provider>;
};

export default { UserContext, UserContextConsumer, UserContextProvider };
