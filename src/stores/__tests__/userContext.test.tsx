// NPM
import React from "react";
import { fireEvent, render } from "@testing-library/react";

// Project
import { UserContext, UserContextProvider, IUserContext } from "../userContext";

describe("UserContext", () => {
  describe("setValue()", () => {
    it("should set firstName to Bob", () => {
      // Arrange
      const assignedFirstName = "Bob";
      const { getByTestId } = render(
        <UserContextProvider>
          <UserContext.Consumer>
            {({ state, actions }: IUserContext) => {
              return (
                <>
                  <span data-testid="action" onClick={() => actions.setFirstName(assignedFirstName)}></span>
                  <span data-testid="result">{state.firstName}</span>
                </>
              );
            }}
          </UserContext.Consumer>
        </UserContextProvider>
      );
      // Act
      fireEvent.click(getByTestId("action"));
      const actual = getByTestId("result").textContent;
      // Assert
      const expected = assignedFirstName;
      expect(actual).toEqual(expected);
    });
  });
});
