// NPM
import React from "react";
import { render } from "@testing-library/react";

// Project
import { UserBadge } from "../userBadge";

describe("Component: UserBadge", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<UserBadge />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
