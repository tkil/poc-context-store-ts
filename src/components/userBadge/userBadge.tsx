import React, { useContext } from "react";
import { IUserContextState, UserContext, userContextEmptyState } from "@stores/userContext";

import "./userBadge.css";

interface IProps {
  userData?: IUserContextState;
}

export const UserBadge: React.FC<IProps> = ({ userData }: IProps) => {
  return (
    <section className="user-badge">
      <div className="user-badge__avatar"></div>
      <div className="user-badge__info">
        <label>
          Name:
          <span>
            {userData?.firstName} {userData?.lastName}
          </span>
        </label>
        <label>
          Email:
          <span>{userData?.email}</span>
        </label>
        <label>
          Phone:
          <span>{userData?.phone}</span>
        </label>
      </div>
    </section>
  );
};

UserBadge.defaultProps = {
  userData: userContextEmptyState,
};

export const UserBadgeConnected = () => {
  const userContext = useContext(UserContext);
  return <UserBadge userData={userContext.state}></UserBadge>;
};
