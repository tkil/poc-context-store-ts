import React, { useContext } from "react";
import {
  IUserContextActions,
  IUserContextState,
  UserContext,
  userContextEmptyActions,
  userContextEmptyState,
} from "@stores/userContext";
import "./userForm.css";

interface IProps {
  userData?: IUserContextState;
  userActions?: IUserContextActions;
}

export const UserForm: React.FC<IProps> = ({ userData, userActions }: IProps) => {
  const handleFirstNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    userActions?.setFirstName(event.target.value);
  };
  const handleLastNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    userActions?.setLastName(event.target.value);
  };
  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    userActions?.setEmail(event.target.value);
  };
  const handlePhoneChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    userActions?.setPhone(event.target.value);
  };

  return (
    <form className="user-form">
      <label>
        First Name:
        <input name="first-name" value={userData?.firstName} onChange={handleFirstNameChange}></input>
      </label>
      <label>
        Last Name:
        <input name="last-name" value={userData?.lastName} onChange={handleLastNameChange}></input>
      </label>
      <label>
        Email:
        <input name="email" value={userData?.email} onChange={handleEmailChange}></input>
      </label>
      <label>
        Phone:
        <input name="phone" value={userData?.phone} onChange={handlePhoneChange}></input>
      </label>
    </form>
  );
};

UserForm.defaultProps = {
  userData: userContextEmptyState,
  userActions: userContextEmptyActions,
};

export const UserFormConnected = () => {
  const userContext = useContext(UserContext);
  return <UserForm userActions={userContext.actions} userData={userContext.state}></UserForm>;
};
