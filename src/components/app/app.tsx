import React from "react";
import { UserContextProvider, IUserContextState } from "@stores/userContext";
import { UserFormConnected } from "@components/userForm/userForm";
import { UserBadgeConnected } from "@components/userBadge/userBadge";

import "./app.css";

export const App: React.FC = () => {
  const initialState: IUserContextState = {
    firstName: "Sally",
    lastName: "Green",
    phone: "555-555-2000",
    email: "sallygreenfake@carfax.com",
  };

  return (
    <UserContextProvider initialState={initialState}>
      <div className="app">
        <header className="app__header">
          <h1 className="app__title">Context Store Demo</h1>
        </header>
        <UserFormConnected />
        <UserBadgeConnected />
      </div>
    </UserContextProvider>
  );
};
